// 发送Ajax请求
import config from "./config";
// 拿到数据的是request.js 最终用的是index.js 所以要想办法返回
export default (url,data={},method='GET')=>{
    return new Promise((resolve, reject)=>{
        // 1.初始化promise实例的状态为pending(后面要修改成功状态)
        wx.request({
            // es6同名属性可以省略不写
            url:config.host+url,
            data,
            method,
            success:(res)=>{
                // console.log("请求成功：",res)
                // 把异步数据传出去  resolve修改promise的状态为resolved
                resolve(res.data)
                // console.log(res.data)
            },
            fail:(err)=>{
                // console.log("请求失败",err)
                //  reject修改promise的状态为 rejected
                reject(err)
            }
        })
    })

}