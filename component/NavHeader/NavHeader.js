Component({
    /**
     * 组件属性列表，由组件外部传入的数据，和vue的props传参差不多
     */
    properties: {
        title: {
            type: String,
            value: 'title初始值'
        },
        nav: {
            type: String,
            value: 'nav初始值'
        }
    },
    data: {},
    methods: {}
});
