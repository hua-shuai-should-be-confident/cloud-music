import request from "../../utils/request";


Page({

    /**
     * 页面的初始数据
     */
    data: {
        bannerList: [], //轮播图数据
        recommendList: [], //推荐歌单
        topList: [], //排行榜数据

    },

    /**
     * 生命周期函数--监听页面加载
     */
    async onLoad() {
        // wx.request({
        //   url:'http://localhost:3000/banner',
        //   data:{type:2},
        //   success:(res)=>{
        //     console.log("请求成功：",res)
        //   },
        //   fail:(err)=>{
        //     console.log("请求失败",err)
        //   }
        // })
        // await等待异步请求，异步任务要返回promise实例（所以当前功能函数的返回值应该是一个promise）
        let bannerListData = await request("/banner", {type: 2})
        // console.log("返回的数据：" + bannerListData)
        this.setData({
            bannerList: bannerListData.banners
        })
        // 推荐歌单数据
        let recommendListData = await request("/personalized", {limit: 12})
        this.setData({
            recommendList: recommendListData.result
        })

        /*
        * /topList 获取所有排行榜单
        * detailList 歌单详情
        * 先发请求/toplist获取所有榜单,然后获取到这些榜单的id,再发/playlist/detail携带榜单的id获取到歌单详情,
        * */
        let allTopListData = await request('/toplist')
        let topList = allTopListData.list.slice(0, 5)
        let topListDetail = []
        for (let item of topList) {
            let detailList = await request(`/playlist/detail?id=${item.id}`, {limit: 5})
            topListDetail.push({name: detailList.playlist.name, tracks: detailList.playlist.tracks.slice(0, 10)})
            this.setData({
                topList: topListDetail
            })
        }


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})
